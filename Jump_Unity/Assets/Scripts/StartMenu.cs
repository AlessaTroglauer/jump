using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour
{
    //references to scenes
    [SerializeField] private string sceneOne;
    [SerializeField] private string sceneTwo;
    [SerializeField] private string sceneThree;

    //Reference to aniamtor 
    [SerializeField] private Animator transition;

    //Adjustable variable for time of transition
    [SerializeField] private float transitionTime;

    //Load scenes
    public void LoadLevel_01()
    {
        StartCoroutine(LoadLevel(sceneOne));
    }

    public void LoadLevel_02()
    {
        StartCoroutine(LoadLevel(sceneTwo));
    }

    public void LoadLevel_03()
    {
        StartCoroutine(LoadLevel(sceneThree));
    }

    //Set bool of animation for fade effect and load scene after time 
    public IEnumerator LoadLevel(string levelToLoad)
    {
        transition.SetTrigger("Start");

        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene(levelToLoad);
    }

    //Quit game
    public void QuitGame()
    {
        Application.Quit();
    }
}
