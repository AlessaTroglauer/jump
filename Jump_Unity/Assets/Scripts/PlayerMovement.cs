using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //reference to character controller 
    public CharacterController controller;

    //Adjustable variables for player movement 
    public float speed = 12f;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;
    public float jumpPadHeight = 10f; 

    //variables for groundcheck 
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask; 

    private Vector3 velocity;
    private bool isGrounded; 

    void Update()
    {
        //Set if palyer is grounded and change velocity based on that 
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask); 

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f; 
        }

        //Get player input and set variables 
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        //Move player 
        controller.Move(move * speed * Time.deltaTime);

        //Check for player jump input 
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            //Jump
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime); 
    }

    //Check if player hits jump pad and change their velocity for boost
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        switch (hit.gameObject.tag)
        {
            case "JumpPad":
                velocity.y = Mathf.Sqrt(jumpPadHeight * -2f * gravity);
                break;
        }
    }
}
