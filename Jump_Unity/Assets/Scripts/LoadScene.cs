using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class LoadScene : MonoBehaviour
{
    //rerfernce to scene 
    [SerializeField] private string sceneNameToLoad;

    //reference animator 
    [SerializeField] private Animator transition;

    //Adjustable variable for duration of transition
    [SerializeField] private float transitionTime;

    //Load level when entering trigger
    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(LoadLevel(sceneNameToLoad));
    }

    //Call coroutine
    public void LoadNextLevel()
    {
        StartCoroutine(LoadLevel(sceneNameToLoad));
    }

    //Set animator bool for fading and load scene after time 
    private IEnumerator LoadLevel(string levelToLoad)
    {
        transition.SetTrigger("Start");

        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene(levelToLoad);
    }
}
